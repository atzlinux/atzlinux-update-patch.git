#!/bin/bash
# atzlinux background image for gnome-background-properties xml files

GBPPATH=usr/share/gnome-background-properties/
mkdir -pv $GBPPATH

for f in /usr/share/backgrounds/atzlinux/*;
do
xmlfilename=`basename "$f"|awk -F"." {'print $1'}`

sed "s%BGPATH%$f%" gnome-background-properties.xml.templet | sed "s%BGNAME%atz-$xmlfilename%" > $GBPPATH/atz-$xmlfilename.xml

done
